import React from 'react';

function DisplayOctal(props) {
  return (
    <div className="DisplayContainer">
      <div className="Placeholder">Oct</div>
      <div className="Placeholder">{props.value.toString(8)}</div>
    </div>
  );
}

export default DisplayOctal;