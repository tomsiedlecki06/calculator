import React from 'react';
import './styles/DisplayStyles.css';

function DisplayDecimal(props) {
  return (
    <div className="DisplayContainer">
      <div className="Placeholder">Dec</div>
      <div className="Placeholder">{props.value}</div>
    </div>
  );
}

export default DisplayDecimal;