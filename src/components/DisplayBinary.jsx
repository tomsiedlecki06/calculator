import React from 'react';

function DisplayBinary(props) {
  return (
    <div className="DisplayContainer">
      <div className="Placeholder">Bin</div>
      <div className="Placeholder">{props.value.toString(2)}</div>
    </div>
  );
}

export default DisplayBinary;