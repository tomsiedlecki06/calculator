import React from 'react';
import './styles/ButtonStyles.css';

function Keypad(props) {

    //co wyswietla przycisk
    let text;
    if(props.type === "=")
    {
        text = "="
    }
    else if(props.type === "operator" || props.type === "number")
    { 
        text = props.value;
    }
    else if(props.type === "clear")
    {
        text = "C";
    }
    else
    {
        text = null;
    }
    //---------------------

  const handleClick = () => {
    if(props.type === "=")
    {
        let value = props.FromNotation(props.ToNotation(props.operation));
        props.onButtonClick(value);
          
    }
    else if(props.type === "clear")
    {
        props.onButtonClick("");
    } 
    else if(props.type === "number")
    { 
        props.onButtonClick(props.operation+props.value);
    }
    else if(props.type === "operator")
    {
        console.log("OPERATOR");
        if(props.operation.length > 0)
        {
            //console.log(props.operation);
            //console.log("ostatni");
            let char = props.operation[props.operation.length -1];
            console.log(char);
            if(char >= '0' && char <= '9')
            {
                console.log("jest lczba");
                props.onButtonClick(props.operation+props.value);
            }
            else if(char == ")")
            {
                console.log("jest )");
                if(props.value != "(")
                {
                    props.onButtonClick(props.operation+props.value);
                }   
            }
            else if(char != "(")
            {
                console.log("jest +-/*");
                // let value = props.operation;
                if(props.value != "(")
                {
                    const value = props.operation.slice(0,-1) + props.value;
                    props.onButtonClick(value);                    
                }
                else
                {
                    props.onButtonClick(props.operation+props.value);
                }
            }

        }
        else
        {
            if(props.value == "(")
            {
                props.onButtonClick(props.operation+props.value);
            }
        }
    }
    else
    {
        props.onButtonClick("");
    }
  };

  return (
        <button onClick={handleClick} className="buttonstyle">{text}</button>
  );
}

export default Keypad;