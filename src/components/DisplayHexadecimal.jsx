import React from 'react';

function DisplayHexadecimal(props) {
  return (
    <div className="DisplayContainer">
      <div className="Placeholder">Hex</div>
      <div className="Placeholder">{props.value.toString(16)}</div>
    </div>
  );
}

export default DisplayHexadecimal;