import './App.css';
import DisplayDecimal from './components/DisplayDecimal';
import DisplayBinary from './components/DisplayBinary';
import DisplayHexadecimal from './components/DisplayHexadecimal';
import DisplayOctal from './components/DisplayOctal';
import Keypad from './components/Keypad';
import Information from './components/Information';
import { useState } from 'react';

function App (){
  const [value, setValue] = useState(2137);
  const [operation, setOperation] = useState("");
  const [valueTMP, setValueTMP] = useState(2137);

  const updateValue = (newValue) => {
    setValue(newValue);
  };
  //---------------------------------------
  const ToNotation = (notation) =>{
    const regex = /(\d+|[()+\-*\/])/g;
    const tokens = notation.match(regex);

    let stos = [];
    let result = [];

    const prio  = {
      "(":0,
      "+":1,
      "-":1,
      ")":1,
      "*":2,
      "/":2
    };
//console.log("dsdaa");
//console.log(tokens);
    tokens.forEach((token) => {
      if(token !== "(" && token !== ")" && token !== "+" && token !== "-" && token !== "*" && token !== "/")
      {
        result.push(token);
      }
      else
      {
        if(stos.length === 0)
        {
          stos.push(token);
        }
        else
        {
          if(token == ")")
          {
            for(let i = stos.length -1 ; i >= 0; i--)
            {
              //console.log(stos[i]);
              if(stos[i] != "(")
              {
                result.push(stos[i]);
              }
              else
              {
                break;
              }
            }
            for(let i = stos.length -1 ; i >= 0; i--)
            {
              //console.log(stos[i]);
              if(stos[i] != "(")
              {
                stos.splice(i, 1);
              }
              else
              {
                stos.splice(i, 1);
                break;
              }
            }
          }
          else if (token == "(")
          {
            stos.push(token);
          }
          else if(prio[token] > prio[stos[stos.length-1]])
          {
            stos.push(token);
          }
          else
          {
            result.push(stos[stos.length-1]);
            stos.splice(stos.length-1,1);

            if(stos.lenght > 0)
            {
              if(prio[token] <= prio[stos[stos.length-1]])
              {
                result.push(stos[stos.length-1]);
                stos.splice(stos.length-1,1);
              }
            }
            stos.push(token);
          }
        }
      }
    });
    if(stos.length !== 0)
    {
      for(let i = stos.length -1 ; i >= 0; i--)
      {
        result.push(stos[i]);
      }
    }
    //console.log("---------------------------");
    //console.log(result);
    //console.log(stos);
    return result
  };

  const FromNotation = (notation) =>{
    for(let i = 0; i < notation.length; i++){
      //liczba
      if(notation[i] !== "(" && notation[i] !== ")" && notation[i] !== "+" && notation[i] !== "-" && notation[i] !== "*" && notation[i] !== "/")
      {

      }
      //nie liczba
      else
      {
        let result;
        if(notation[i] == "+")
        {
          result = parseInt(notation[i-2]) + parseInt(notation[i-1]);
        }
        else if(notation[i] == "-")
        {
          result = parseInt(notation[i-2]) - parseInt(notation[i-1]);
          
          
        }
        else if(notation[i] == "*")
        {
          result = parseInt(notation[i-2]) * parseInt(notation[i-1]);


        }
        else if(notation[i] == "/")
        {
          result = parseInt(notation[i-2]) / parseInt(notation[i-1]);

        }
        notation.splice(i - 2, 3, result.toString());
        i = 0;
      }
    }
    return parseInt(notation[0]);
  }
  return (
    <div className="App">
      <DisplayHexadecimal value={value}/>ed
      <DisplayDecimal value={value}/>
      <DisplayOctal value={value}/>
      <DisplayBinary value={value}/>
      <div className='buttons'>
      <Keypad onButtonClick={setOperation} operation={operation} type="operator" value="("/>
      <Keypad onButtonClick={setOperation} operation={operation} type="operator" value=")"/>
      <Keypad onButtonClick={setOperation} operation={operation} type="operator" value="/" />
      <Keypad onButtonClick={setOperation} operation={operation} type="operator" value="*"/>
      <Keypad onButtonClick={setOperation} operation={operation} type="operator" value="-"/>
      <Keypad onButtonClick={setOperation} operation={operation} type="operator" value="+"/>
      <Keypad onButtonClick={setOperation} operation={operation} type="number" value="1"/>
      <Keypad onButtonClick={setOperation} operation={operation} type="number" value="2"/>
      <Keypad onButtonClick={setOperation} operation={operation} type="number" value="3"/>
      <Keypad onButtonClick={setOperation} operation={operation} type="number" value="4"/>
      <Keypad onButtonClick={setOperation} operation={operation} type="number" value="5"/>
      <Keypad onButtonClick={setOperation} operation={operation} type="number" value="6"/>
      <Keypad onButtonClick={setOperation} operation={operation} type="number" value="7"/>
      <Keypad onButtonClick={setOperation} operation={operation} type="number" value="8"/>
      <Keypad onButtonClick={setOperation} operation={operation} type="number" value="9"/>
      <Keypad onButtonClick={updateValue} type="=" FromNotation={FromNotation} ToNotation={ToNotation} operation={operation}/>
      <Keypad type="clear" operation={operation} onButtonClick={setOperation} value="C"/>
      </div>
      <Information operation={operation}/>
    </div>
  );
}

export default App;
